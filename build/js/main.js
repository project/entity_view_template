(function ($, Drupal, once) {
  Drupal.behaviors.codeMirrorEntityViewTemplate = {
    attach: function (context, settings) {
      once('codeMirrorEntityViewTemplate', '#entity-template-view', context).forEach(function (element) {
        var editor =
          CodeMirror.fromTextArea(element, {mode:
              {name: "twig", base: "text/html"}});
      });
    }
  };
})(jQuery, Drupal, once);
